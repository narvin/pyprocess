"""Run async processes."""
from __future__ import annotations

import asyncio
import shlex
import typing

RunResult = tuple[asyncio.subprocess.Process, str, str]


async def _run_process(proc: asyncio.subprocess.Process, inp: str | None = None) -> tuple[str, str]:
    """Run a process: send it input, get its output, and wait for it to finish.

    Args:
        proc: The created process.
        inp: Input that will be written to the process' stdin.

    Returns:
        Strings of what was written to stdout and stderr.

    """
    stdout, stderr = await proc.communicate(inp is not None and inp.encode() or None)
    return stdout.decode(), stderr.decode()


async def run_exec(program: str, *args: str, inp: str | None = None) -> RunResult:
    r"""Run a program asynchronously.

    Args:
        program: The name of the program to run.
        args: Arguments to pass to the program.
        inp: Input that will be written to the process' stdin.

    Returns:
        The process object, and strings of what was written to stdout and stderr.

    Examples:
        Run python and get its version.

        >>> async def test():
        ...     res = await run_exec("python", "--version")
        ...     print(res)
        >>> asyncio.run(test())
        (<Process ...>, 'Python 3...\n', '')

        Run python and send it commands on stdin.

        >>> async def test():
        ...     inputs = [
        ...         "print('hello')",
        ...         "print(f'{1+1=}')",
        ...     ]
        ...     res = await run_exec("python", inp="\n".join(inputs))
        ...     print(res)
        >>> asyncio.run(test())
        (<Process ...>, 'hello\n1+1=2\n', '')

    """
    proc = await asyncio.create_subprocess_exec(
        program,
        *args,
        stdin=asyncio.subprocess.PIPE if inp else None,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE,
    )
    return proc, *(await _run_process(proc, inp))


async def run_shell(cmd: str, *, inp: str | None = None) -> RunResult:
    r"""Run a shell command asynchronously.

    Args:
        cmd: The full shell command to run, which should be properly escaped.
        inp: Input that will be written to the process' stdin.

    Returns:
        The process object, and strings of what was written to stdout and stderr.

    Examples:
        Run the echo shell command.

        >>> async def test():
        ...     res = await run_shell("echo foo")
        ...     print(res)
        >>> asyncio.run(test())
        (<Process ...>, 'foo\n', '')

        Run python, send it commands on stdin, and pipe the output to grep.

        >>> async def test():
        ...     inputs = [
        ...         "print('hello')",
        ...         "print(f'{1+1=}')",
        ...     ]
        ...     res = await run_shell("python | grep hello", inp="\n".join(inputs))
        ...     print(res)
        >>> asyncio.run(test())
        (<Process ...>, 'hello\n', '')

    """
    proc = await asyncio.create_subprocess_shell(
        cmd,
        stdin=asyncio.subprocess.PIPE if inp else None,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE,
    )
    return proc, *(await _run_process(proc, inp))


class Command(typing.Protocol):
    """Callable protocol for a command function."""

    async def __call__(self: typing.Self, args: str | None, *, inp: str | None) -> RunResult:
        """The type of a command function."""


def make_cmd(cmd: str) -> Command:
    r"""Make a shell command function that can be called with the command's remaining args.

    Args:
        cmd: The command name.

    Returns:
        A function that can be called with arguments and input to execute the command in a shell.

    Examples:
        >>> async def test():
        ...     echo = make_cmd("echo")
        ...     res = await echo("foo")
        ...     print(res)
        >>> asyncio.run(test())
        (<Process ...>, 'foo\n', '')

    """

    async def cmd_fn(args: str | None = None, *, inp: str | None = None) -> RunResult:
        cmd_with_args = f"{cmd} {shlex.quote(args)}" if args else cmd
        return await run_shell(cmd_with_args, inp=inp)

    return cmd_fn


class RunOutput(typing.NamedTuple):
    """The output from running a process."""

    code: int | None
    out: list[str]
    err: list[str]


def to_output(res: RunResult) -> RunOutput:
    """Convert a ``RunResult`` to a ``RunOutput``.

    If stdout or stderr contain a trailing blank line, it is omitted from the output.

    Args:
        res: The ``RunResult``.

    Returns:
        A ``RunOutput`` generated from res.

    Examples:
        Run a command that produces lines on stdout and stderr.

        >>> async def test():
        ...     res = await run_shell("echo foo && echo bar && echo baz >&2")
        ...     out = to_output(res)
        ...     print(out)
        >>> asyncio.run(test())
        RunOutput(code=0, out=['foo', 'bar'], err=['baz'])

    """

    def to_list(msg: str) -> list[str]:
        ls = msg.split("\n") if msg else []
        return ls[:-1] if ls and ls[-1] == "" else ls

    proc, out, err = res
    return RunOutput(code=proc.returncode, out=to_list(out), err=to_list(err))


if __name__ == "__main__":
    import doctest

    doctest.testmod(optionflags=doctest.ELLIPSIS)
